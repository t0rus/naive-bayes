import com.taylorbockman.ml._


//Small test stub to demonstrate the classifier

object TestStub {
  
  def main(args: Array[String]): Unit = {
    //Keep building up the stub to eventually show probabilities
    //of being correct, etc, after all test printlns are removed from
    
    //Instantiate and train the NB Classifier.
    val testnb = NaiveBayes()
    testnb.train("Hey joe, saw your email. Lunch at noon?", "good")
    testnb.train("J0e ready for hot dates tonight?", "bad")
    testnb.train("Sweet baby ray's look at this video!", "good")
    testnb.train("Test Bank needs you to update your information", "bad")
    testnb.train("Cialis casino dont gamble on your viagra", "bad")

    //Test for probability
    val cialisProb = testnb.conditionalProb("cialis","bad")
    val emailProb  = testnb.conditionalProb("email", "bad")

    //assert(cialisProb == .33, "Check conditional probability (cialisProb == " + cialisProb + ")")
    //assert(emailProb == 0, "Check conditional probabilitity (emailProb == " + emailProb + ")")

    val wCialisProb = testnb.weightedProb("cialis", "bad")
    val wEmailProb = testnb.weightedProb("email", "bad")
   

    //Hand check these probabilities before continuing
    println("Cialis Weighted Probability = " + wCialisProb)
    println("Email Weighted Probability = " + wEmailProb)
  }


}
