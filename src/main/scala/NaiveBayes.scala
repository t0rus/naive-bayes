package com.taylorbockman.ml

import collection.mutable.HashMap


/* 
 * Naive Bayes Classifer
 * (c) 2014 Taylor Bockman
 *
 *  Can be persisted via mongoDB integration for speed.
 *
 *  Premise:
 *   The goal is to find P(C|D) where C is a category and D is a document.
 *   Given Baye's Theorem, we can do this by doing
 *   P(C|D) = P(Di|C)P(C) * ... * P(Dn|C)P(C). 
 *   
 *   The denominator of Baye's Theorem is not needed because the probability of
 *   being a document does not depend on the category. That is, P(D) is the same
 *   for all categories under test.
 *
 *
 *   
 *   TODO:
 *
 *   Also, add a function that shows the probability the choice is correct.
 *   There's a way to do it. Figure it out. Alternatively make the classify function
 *   return a pair (class, prob_correct).
 *
 *   Add logarithmic smoothing to prevent underflow for extremely small probabilities.
 *   Persist probabilities in a variable to save recalculation. Only recalculate probabilities when
 *   retraining. Otherwise just looking up the probabilities.
*/

 class NaiveBayes() {
  
  val featurec  = new HashMap[(String, String), Int]
  val categoryc = new HashMap[String, Int]


  // NaiveBayes Train()
  // Takes a document string and a category and updates featurec and categoryc.
  //
  // Algorithm Note: Train takes a "Document", a series of tokens essentially.
  //                 it tokenizes the document and places the documents in the category
  //                 or increments the current count of they exist.
  //                 Finally, it increments category count to represent that a document
  //                 (remember, a series of tokens) has been classified as the category.
  //                 It only increments category once because it is only concerned about
  //                 the probability of being a document in the category. Since
  //                 what we will be classifying for the user are documents, not
  //                 individual words.
  //
  // 
  //
  def train(doc: String, category: String)  = {
    val tok = new Tokenizer
    val tokens: List[String] = tok.tokenize(doc)
    
    tokens.foreach(incFeature(_, category))
    incCategory(category)
    
  }

  
  // Takes a feature and category and increments the associated number
  // Returns a new instance of the featurec hashmap that includes the addition
  // of the feature.
  private def incFeature(feature: String, category: String) = {
    featurec += (((feature, category),featurec.getOrElse((feature,category),0) + 1)) 
  }

  //Increment the count for a category
  //to recognize a document is using it.
  private def incCategory(category: String) = {
    categoryc += ((category, categoryc.getOrElse(category, 0) + 1))
  }


  // Returns an iterable of the categories
  private def categories = {
   categoryc.keys
  }

  //Simply returns the count of occurences for the (feature,category)
  //pair.
  private def featureCount(feature: String, category: String) = { 
    featurec.getOrElse((feature,category),0)
  }

  //Simply returns the count of times documents have appeared in 
  //the requested category.
  private def categoryCount(category: String) = {
    categoryc.getOrElse(category,0)
  }
  
  //Calculates the conditional probability of a feature given a category. That is:
  // Let D = Document, Then Di is the ith feature of the document
  // Let C = Category
  // Then it follows by conditional probability:
  // P(Di|C) = P(Di & C)/P(C)
  //
  // Left public for testing mathematical soundness, and it does not effect the
  // state of the NaiveBayes object.
  //
  def conditionalProb(feature: String, category: String): Double = {
   
    categoryCount(category) match {
      case 0 => 0
      case catCount: Int => featureCount(feature,category).toDouble/catCount 
    }

  }

  //Calculates the weighted probability of a feature given a category.
  //
  //The idea is simple: Naive Bayes is heavily influenced during early training.
  //By weighting the initial probabilities, features that dont appear in a 
  //category many times many still be considered for that category until more data
  //proves otherwise.
  //
  //EXPLANATION:
  //
  //If we multiply weight*assumedProb we get our weighted assumed prob.
  //Then we add that to the condProb weighted by the feature's total appearances
  //Finally, weight the total occurences and divide by them (since that is 
  //what it is out of)
  //
  //A weight of 1.0 means the weight of 1 word. The assumed probability is the 
  //initial probability we choose to assign to every word as a default rather than
  //0 (the raw probability of zero occurences).
  //
  //Left public for testing mathematical soundness
  //
  def weightedProb(feature: String, category:String, 
                   weight: Double = 1.0, assumedProb: Double = 0.5): Double = {
   
    var condProb: Double = conditionalProb(feature, category) 
    
    //Total amount of times this feature appears in all categories
    var totalCount: Int = categories.foldLeft(0)((acc, cat) => acc + featureCount(feature, cat))
    println(condProb)
    ((weight * assumedProb) + (totalCount*condProb)).toDouble/(weight+totalCount)
  }
  
  private def totalItems = {
    categoryc.foldLeft(0)((acc, item) => acc + item._2)
  }  
}

object NaiveBayes {
  def apply(): NaiveBayes = new NaiveBayes
}
